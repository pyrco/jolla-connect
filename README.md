# Automatically run scripts when connecting your Jolla to your laptop with USB
This is a write up on how to automatically run the SSH server on your Jolla when plugging it in to another machine, for example your laptop, using USB and then run some script on this machine. This setup can be useful to run for example file syncing or backup scripts that use SSH to do their job.

The reason for the USB-only approach is that it is relatively easy to detect an SSH connection is possible and to have the automatic SSH access somewhat hardened. You can use a wlan or mobile data connection to periodically backup or sync files, but these networks may not be yours or less trusted or not have access to the backup or file syncing server you use. To get to the same level of trust on these types of connections, a lot more pieces need to be setup. Also some way to make sure the periodic file sync o backup does not drain your Jolla of its last power is probably needed.

As an added bonus this setup will also give you an easy way to SSH into your Jolla when plugging it into a laptop without having to push too many buttons or go through too many manual configuration steps each time.

This write up probably assumes some agility with using/administering Linux based machines. It also assumes you have enabled *Developer Mode* on your Jolla and are able to SSH into it using the default *Remote connection* option. Furthermore it assumes the machine you plug your Jolla into uses systemd as its init system, also some file locations may be Ubuntu 16.04 specific.

The examples here are specific for the Jolla 1, but are easily adapted for any other SailfishOS device or even generic devices, just mix the bits and pieces you need.

## Overview

A short summary of the steps needed to get everything working:

* Start the SSH server on the Jolla device when the USB network connection is made
* Create a persistent network device name for the Jolla USB network on the target machine (e.g. your laptop)
* Automatically start some script on the target machine when the USB network is activated

## SSH server on the Jolla
Sailfish OS comes pre-installed with the openssh SSH server. The 'normal' way to activate it, is to have *Developer mode* enabled on the device and enable the *Settings app -> Developer tools -> Remote connection*. After providing your security code it will start the SSH server by starting the `sshd.socket` systemd unit. It will also generate and set a password for the nemo user, which can be used to login over SSH and become root using the `devel-su` command.

### Hardening the SSH server
The default setting for the SSH server is to listen on all network devices for incoming connections (`:::22`). It also allows password authentication for the nemo user. In this section we're going to address both these items, the latter first.

#### Use SSH keys
Make sure you have SSH public and private keys set up for the user you are using on your target machine. Add your public SSH key to the `/home/nemo/.ssh/authorized_keys` file on the Jolla. Do the same for the root user in `/root/.ssh/authorized_keys`. Once the SSH server is setup to run automatically when the USB connection becomes available, this will allow you to login to your Jolla device without having to set a password by enabling the *Remote connection* in the *Settings app*. It also allows you to login as root, which is needed since `devel-su` will not work if there is no password set for the nemo user.

The SSH server in Sailfish is configured to allow password authentication. Since no passwords will be set when everything is configured according to the steps described in this write up, this setting becomes kind of a no-op. However if you want to be extra sure password authentication will never be possible, add `PasswordAuthentication no` to the `/etc/ssh/sshd_config` file.

#### Have the SSH server listen on a specific network device
systemd allows you to override specific configuration values of services without touching the other system/distribution provided settings. Run `systemctl edit sshd.socket` as root on your Jolla device and add the contents:

```
[Socket]
BindToDevice=rndis0
```

This will create the file `/etc/systemd/system/sshd.socket.d/override.conf`, which will make the SSH server listen only on the USB network device. It uses the SO_BINDTODEVICE socket option when setting up the listening socket to accomplish this.

> In this case `rndis0` is the name of the USB network device on the Jolla 1, other devices may have different names.

Note that the `BindToDevice` option will automatically set the `After` and `BindsTo` options to `sys-devices-platform-msm_hsusb-gadget-net-rndis0.device` (the value in case of a Jolla 1 device). This ensures the SSH server unit can only be started if the USB network device unit is actually available and will be stopped when it disappears again.

When looking at the output of `netstat` or `ss`, you will see the socket is still bound to the any IP address `::`, so not much will seem to have changed. However, because of the SO_BINDTODEVICE socket option, any connection attempt not coming in on the device defined in `BindToDevice` will be responded to with a TCP RST.

Another option is to use the `ListenStream` option and define specific IP addresses to bind to. This option can be set multiple times, so multiple addresses assigned to different devices can be used. Make sure to first clear the setting from previous values and to set the `FreeBind` option to true. The latter allows for binding to an IP address while it is not yet setup on a device (which is often the case on appearing and disappearing USB network devices). Example:

```
[Socket]
ListenStream=
ListenStream=192.168.2.15:22
FreeBind=true
```

> 192.168.2.15 is the address automatically used for the rndis network device on the Jolla.

Since `ListenStream` does not depend on any specific device but only on IP addresses / ports, no `After` or `BindsTo` options need to be set.

A third option would be to create multiple, differently named `.socket` units, which each use their own, different value for the `BindsTo` option.

Make sure to do a `systemctl daemon-reload` so the changes are picked up by systemd.

### Starting the SSH server when the USB network device becomes available
By default systemd dynamically creates `.device` units for network devices. To have the SSH server start when such a `.device` unit (and thus the network) becomes available, a dependency from that unit on the `sshd.socket` unit needs to be declared. Because the device unit is created dynamically, no actual file describing and configuring it exists. So the easiest way to specify such a dependency is by doing it in reverse, declaring the dependency in the `sshd.socket` unit by using the `WantedBy` configuration option. On your Jolla device run `systemctl edit sshd.socket` as root and add the following:

```
[Install]
WantedBy=sys-devices-platform-msm_hsusb-gadget-net-rndis0.device
```

> `sys-devices-platform-msm_hsusb-gadget-net-rndis0.device` is the specific rndis device for the Jolla 1, for other devices consult the output from the `systemctl` command for the appropriate value.

After a `systemctl daemon-reload`, enabling the `sshd.socket` unit using `systemctl enable sshd.socket` should create a symlink to the `sshd.socket` unit in `/etc/systemd/system/sys-devices-platform-msm_hsusb-gadget-net-rndis0.device.wants`. However on SailfishOS 2.2.0.29/systemd 225 this does somehow not work automatically, so the symlink needs to be created manually:

```
# mkdir -p /etc/systemd/system/sys-devices-platform-msm_hsusb-gadget-net-rndis0.device.wants
# cd /etc/systemd/system/sys-devices-platform-msm_hsusb-gadget-net-rndis0.device.wants
# ln -s /lib/systemd/system/sshd.socket
```

Now `systemctl daemon-reload` to have the symlink change take effect, plug your Jolla into the target machine with a USB cable, tap the *Developer Mode* choice that appears on your Jolla to enable the rndis USB network device, and you should have an SSH server listening on the USB network interface. Unplug the USB connection and the SSH server socket should go away again.

## Create a persistent network device name on the target machine
When the rndis driver configures a USB network device it uses a different MAC address every time a device is plugged in. The name assigned to the device also differs depending on which bus the device is plugged into and if there are other rndis device already plugged in. To be able to automatically configure the network interface with tools like NetworkManager, a udev rule is needed to rename the device name to something stable, so it can be configured by name instead of MAC address.

Create or update a udev rule file, e.g. `/etc/udev/rules.d/90-local.rules`, and add the following:

```
# Jolla RNDIS fixed device name
SUBSYSTEM=="net", SUBSYSTEMS=="usb", ATTRS{product}=="Jolla", ATTRS{serial}=="JOLLA_SERIAL", NAME="jolla0"
```

This rule will trigger when a device change is detected and all of the `SUBSYTEM` and `ATTRS` properties are true for this device. It will subsequently set the `NAME` for the device to `jolla0`.

To find out the actual value of `JOLLA_SERIAL` for your specific device, use the following command:
```
# lsusb -v -d 2931:0a02|grep iSerial
  iSerial                 3 JOLLA_SERIAL
```

> `2931:0a02` is the Jolla 1 USB vendor and product id, use `lsusb` to find out which ids are applicable if you have a different device.

Replace the `JOLLA_SERIAL` in the udev rule file with the value appearing in the `lsusb` output in the place of `JOLLA_SERIAL`

Now you can setup the network properties for the `jolla0` network device using your favourite network managing tool. Since this is an appearing and disappearing network, NetworkManager seems like a useful choice.

### KDE 5 NetworkManager connection editor quirk
 The NetworkManager GUI tool I used (the KDE 5 NetworkManager connection editor) was not able to create a configuration without a MAC address when restricting it to a specific network device name. Since the MAC address changes every time the device is plugged in, the configuration would not trigger on any newly made connections. To get around this I created the network configuration called `Jolla` for the `jolla0` network device and manually edited the configuration file (`/etc/NetworkManager/system-connections/Jolla`) afterwards, removing the MAC address information:

```
[ethernet]
mac-address=
mac-address-blacklist=
```

The downside is that every time you change and save the configuration using the GUI tool, you have to also manually edit the configuration file, removing the MAC address information again. YMMV with other NetworkManager configuration tools, just make sure no MAC address is configured when restricting it to a network device name.

## Automatically start some script on the target machine when the network is activated
The final step of having some script start automatically when the rndis network device becomes available works in a similar way as starting the SSH server on the Jolla: by using the `WantedBy` option. The only difference in this example is the script will run as the user you are logged in as instead of as root. This is for one good practice: limiting the capabilities of programs as much as possible. It also makes it much more likely SSH keys are available through the ssh-agent which, in my case, is setup on login.

User specific systemd units live in the user's `~/.config/systemd/user` directory. Depending on whether persistent user sessions are enabled or not, they can either always be executed (if the conditions are met), or only if the user is logged in.

To make sure the unit can only be run when the appropriate network is available, the proper `After` and `BindsTo` configuration options need to be set. To have the unit execute when the network device becomes available, the `WantedBy` option is used to declare the dependency of that device on this unit.

The following `~/.config/systemd/user/jolla-backup.service` file accomplishes this:

```
[Unit]
Description=Backup Jolla phone
After=sys-subsystem-net-devices-jolla0.device
BindsTo=sys-subsystem-net-devices-jolla0.device

[Service]
Type=oneshot
ExecStart=/home/user/bin/jolla-backup

[Install]
WantedBy=sys-subsystem-net-devices-jolla0.device
```

> The `sys-subsystem-net-devices-jolla0.device` is the systemd device name for the `jolla0` device we created earlier using the udev rule. If you use a different name, or the layout of the `/sys` filesystem differs on your machine, look at the output of `systemctl` to see what this name should be.

Now, after a `systemctl daemon-reload --user`, enable the unit with `systemctl enable jolla-backup --user`. A symlink from `~/.config/systemd/user/sys-subsystem-net-devices-jolla0.device.wants` to `~/.config/systemd/user/jolla-backup.service` will be created and everything is good to go!

One last hurdle you need to be aware of is the fact that if the network device comes up and your script is started, it will take a moment for it to have an IP address assigned to it. Unfortunately this is not something that can be easily detected and waited on by systemd units. A quick and dirty way to have your script wait for the IP address to be configured for the network is to have a simple poll loop using something like nmcli:

```
while ! (/usr/bin/nmcli -t c show --active "Jolla"|grep IP4.ADDRESS >/dev/null) ; do
  sleep 1;
done
```

[![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)
